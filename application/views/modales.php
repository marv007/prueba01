<!-- modal nueva empresa-->
<div class="modal fade" id="modalNuevaEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form action="" method="POST" id="frmNuevaEmpresa">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva Empresa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <div class="row">
            <div class="col-6">
              <label for="id" class="col-form-label">ID:</label>
              <input type="text" class="form-control"  name="idEmpresa" id="idEmpresa">

              <label for="nombre" class="col-form-label">Nombre:</label>
              <input type="text" class="form-control" name="nombreEmpresa" id="nombreEmpresa" required>

              <label for="nit" class="col-form-label">NIT:</label>
              <input type="text" class="form-control" name="nit" id="nit" required>

              <label for="telefono" class="col-form-label">Teléfono:</label>
              <input type="text" class="form-control" name="telefono" id="telefono" required>
            </div>

            <div class="col-6">
              <label for="direccion" class="col-form-label">Dirección:</label>
              <input type="text" class="form-control" name="direccion" id="direccion" required>

              <label for="departamento" class="col-form-label">Departamento:</label>
              <input type="text" class="form-control" name="departamento" id="departamento" required> 

              <label for="municipio" class="col-form-label">Municipio:</label>
              <input type="text" class="form-control" name="municipio" id="municipio" required>

                       
            </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      
      </div>

    </div>
  </div>
  </form>
</div>

<!-- modal ver empresa-->
<div class="modal fade" id="modalVerEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Empresa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-6">
            <label for="id" class="col-form-label">ID:</label>
            <input type="text" class="form-control" id="id" disabled>

            <label for="nombre" class="col-form-label">Nombre:</label>
            <input type="text" class="form-control" id="nombre" disabled>

            <label for="nit" class="col-form-label">NIT:</label>
            <input type="text" class="form-control" id="nit" disabled>

            <label for="telefono" class="col-form-label">Teléfono:</label>
            <input type="text" class="form-control" id="telefono" disabled>
          </div>

          <div class="col-6">
            <label for="direccion" class="col-form-label">Dirección:</label>
            <input type="text" class="form-control" id="direccion" disabled>

            <label for="municipio" class="col-form-label">Municipio:</label>
            <input type="text" class="form-control" id="municipio" disabled>

            <label for="departamento" class="col-form-label">Departamento:</label>
            <input type="text" class="form-control" id="departamento" disabled>          
          </div>
        </div>
        

        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- modal editar empresa-->
<div class="modal fade" id="modalEditarEmpresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<form method="POST" id="frmEditarEmpresa">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Empresa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
      <div class="row">
          
          <div class="col-6">
          
            <label for="id" class="col-form-label">ID:</label>
            <input type="text" class="form-control" name="idEmpresa" id="id">

            <label for="nombre" class="col-form-label">Nombre:</label>
            <input type="text" class="form-control" name="nombreEmpresa" id="nombre">

            <label for="nit" class="col-form-label">NIT:</label>
            <input type="text" class="form-control" name="nit" id="nit">

            <label for="telefono" class="col-form-label">Teléfono:</label>
            <input type="text" class="form-control" name="telefono" id="telefono">
          </div>

          <div class="col-6">
            <label for="direccion" class="col-form-label">Dirección:</label>
            <input type="text" class="form-control" name="direccion" id="direccion">

            <label for="municipio" class="col-form-label">Municipio:</label>
            <input type="text" class="form-control" name="municipio" id="municipio">

            <label for="departamento" class="col-form-label">Departamento:</label>
            <input type="text" class="form-control" name="departamento" id="departamento">          
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
      </div>
    </div>
  </div>
  </form>
</div>

<!-- modal nuevo empleado-->
<div class="modal fade" id="modalNuevoEmpleado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form id="frmNuevoEmpleado">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Empleado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
        <div class="row">
          <div class="col-6">
            <label for="id" class="col-form-label">ID:</label>
            <input type="text" class="form-control" name ="idEmpleado" id="id">

            <label for="nombres" class="col-form-label">Nombres:</label>
            <input type="text" class="form-control" name="nombres" id="nombres">

            <label for="apellidos" class="col-form-label">Apellidos:</label>
            <input type="text" class="form-control" name="apellidos" id="apellidos">

            <label for="nit" class="col-form-label">NIT:</label>
            <input type="text" class="form-control" name="nit" id="nit">
            
          </div>

          <div class="col-6">
            <label for="dui" class="col-form-label">DUI:</label>
            <input type="text" class="form-control" name="dui" id="dui">

            <label for="direccion" class="col-form-label">Estado:</label>
            <select class="form-control" id="estado" name="estado" value="Activo">
              <option value="Activo">Activo</option>
              <option value="Inactivo">Inactivo</option>
            </select>

            <label for="empresas" class="col-form-label">Empresa:</label>
            <select class="form-control" id="empresas" name="Empresas_idEmpresa">
            <?php foreach ($empresas as $empresa){ ?>
              <option value="<?= $empresa->idEmpresa?>" ><?= $empresa->nombreEmpresa?> </option>

            <?php
            }
             ?>
            
            </select>

            <label for="rol" class="col-form-label">Rol:</label>
            <select class="form-control" id="rol" name="Roles_idRol">
            <?php foreach ($roles as $rol){ ?>
              <option value="<?= $rol->idRol?>" ><?= $rol->nombreRol?> </option>

            <?php
            }
             ?>
            
            </select>
                      
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
      
    </div>
  </div>
  </form>
</div>


<!-- modal ver empleado-->
<div class="modal fade" id="modalVerEmpleado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ver Empleado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-6">
            <label for="id" class="col-form-label">ID:</label>
            <input type="text" class="form-control" id="id" disabled>

            <label for="nombres" class="col-form-label">Nombres:</label>
            <input type="text" class="form-control" id="nombres" disabled>

            <label for="apellidos" class="col-form-label">Apellidos:</label>
            <input type="text" class="form-control" id="apellidos" disabled>

            <label for="nit" class="col-form-label">NIT:</label>
            <input type="text" class="form-control" id="nit" disabled>
            
          </div>

          <div class="col-6">
            <label for="dui" class="col-form-label">DUI:</label>
            <input type="text" class="form-control" id="dui" disabled>

            <label for="direccion" class="col-form-label">Estado:</label>
            <select class="form-control" id="estado" value="Activo" disabled>
              <option value="Activo">Activo</option>
              <option value="Inactivo">Inactivo</option>
            </select>

            <label for="empresas" class="col-form-label">Empresa:</label>
            <select class="form-control" id="empresa" disabled>
            <?php foreach ($empresas as $empresa){ ?>
              <option value="<?= $empresa->idEmpresa?>" ><?= $empresa->nombreEmpresa?> </option>

            <?php
            }
             ?>
            
            </select>

            <label for="rol" class="col-form-label">Rol:</label>
            <select class="form-control" id="rol" disabled>
            <?php foreach ($roles as $rol){ ?>
              <option value="<?= $rol->idRol?>" ><?= $rol->nombreRol?> </option>

            <?php
            }
             ?>
            
            </select>
                      
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!-- modal editar empleado-->
<div class="modal fade" id="modalEditarEmpleado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form id="frmEditarEmpleado">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Empleado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-6">
            <label for="id" class="col-form-label">ID:</label>
            <input type="text" class="form-control" name="idEmpleado" id="id">

            <label for="nombres" class="col-form-label">Nombres:</label>
            <input type="text" class="form-control" name="nombres" id="nombres">

            <label for="apellidos" class="col-form-label">Apellidos:</label>
            <input type="text" class="form-control" name="apellidos" id="apellidos">

            <label for="nit" class="col-form-label">NIT:</label>
            <input type="text" class="form-control" name="nit" id="nit">
            
          </div>

          <div class="col-6">
            <label for="dui" class="col-form-label">DUI:</label>
            <input type="text" class="form-control" name="dui" id="dui">

            <label for="direccion" class="col-form-label">Estado:</label>
            <select class="form-control" id="estado" name="estado" value="Activo">
              <option value="Activo">Activo</option>
              <option value="Inactivo">Inactivo</option>
            </select>

            <label for="empresas" class="col-form-label">Empresa:</label>
            <select class="form-control" name="Empresas_idEmpresa" id="empresa">
            <?php foreach ($empresas as $empresa){ ?>
              <option value="<?= $empresa->idEmpresa?>" ><?= $empresa->nombreEmpresa?> </option>

            <?php
            }
             ?>
            
            </select>

            <label for="rol" class="col-form-label">Rol:</label>
            <select class="form-control" id="rol" name="Roles_idRol" >
            <?php foreach ($roles as $rol){ ?>
              <option value="<?= $rol->idRol?>" ><?= $rol->nombreRol?> </option>

            <?php
            }
             ?>
            
            
            </select>          
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>    
  </div>
  </form>
</div>


<!-- modal nuevo rol-->
<div class="modal fade" id="modalNuevoRol" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form id="frmNuevoRol">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-6">
            <label for="idRol" class="col-form-label">ID Rol:</label>
            <input type="text" class="form-control" name="idRol" id="idRol">

            <label for="nombreRol" class="col-form-label">Nombre Rol:</label>
            <input type="text" class="form-control" name="nombreRol" id="nombreRol">

            <label for="descripcionRol" class="col-form-label">Descripción Rol:</label>
            <input type="text" class="form-control" name="descripcionRol" id="descripcionRol">
          </div>

          <div class="col-6">
            <label for="permisos" class="col-form-label">Permisos:</label>
            <input type="text" class="form-control" name="permisos" id="permisos">            

            <label for="Empresas_idEmpresas" class="col-form-label">Empresa:</label>
            <select class="form-control" name="Empresas_idEmpresa" id="Empresas_idEmpresa">
            <?php foreach ($empresas as $empresa){ ?>
              <option value="<?= $empresa->idEmpresa?>" ><?= $empresa->nombreEmpresa?> </option>

            <?php
            }
             ?>
            
            </select>
         
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
  </form>
</div>

<!-- modal ver rol-->
<div class="modal fade" id="modalVerRol" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ver Rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-6">
            <label for="idRol" class="col-form-label">ID Rol:</label>
            <input type="text" class="form-control" id="idRol" disabled>

            <label for="nombreRol" class="col-form-label">Nombre Rol:</label>
            <input type="text" class="form-control" id="nombreRol" disabled>

            <label for="descripcionRol" class="col-form-label">Descripción Rol:</label>
            <input type="text" class="form-control" id="descripcionRol" disabled>
          </div>

          <div class="col-6">
            <label for="permisos" class="col-form-label">Permisos:</label>
            <input type="text" class="form-control" id="permisos" disabled>            

            <label for="Empresas_idEmpresa" class="col-form-label">Empresa:</label>
            <select class="form-control" id="Empresas_idEmpresa" disabled>
            <?php foreach ($empresas as $empresa){ ?>
              <option value="<?= $empresa->idEmpresa?>" ><?= $empresa->nombreEmpresa?> </option>

            <?php
            }
             ?>
            
            </select>
         
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- modal editar rol-->
<div class="modal fade" id="modalEditarRol" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form id="frmEditarRol">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
          <div class="col-6">
            <label for="idRol" class="col-form-label">ID Rol:</label>
            <input type="text" class="form-control" name="idRol" id="idRol">

            <label for="nombreRol" class="col-form-label">Nombre Rol:</label>
            <input type="text" class="form-control" name="nombreRol" id="nombreRol">

            <label for="descripcionRol" class="col-form-label">Descripción Rol:</label>
            <input type="text" class="form-control" name="descripcionRol" id="descripcionRol">
          </div>

          <div class="col-6">
            <label for="permisos" class="col-form-label">Permisos:</label>
            <input type="text" class="form-control" name="permisos" id="permisos">            

            <label for="Empresas_idEmpresas" class="col-form-label">Empresa:</label>
            <select class="form-control" name="Empresas_idEmpresa" id="Empresas_idEmpresa">
            <?php foreach ($empresas as $empresa){ ?>
              <option value="<?= $empresa->idEmpresa?>" ><?= $empresa->nombreEmpresa?> </option>

            <?php
            }
             ?>
            
            </select>
         
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
  </form>
</div>