<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Empresas</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Empleados</a>
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Roles</a>
                        
                    </div>
                </div>
                <div class="col-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">Empresas
                            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalNuevaEmpresa">+ Nuevo</button>
                            <table class="table table-hover " id="tablaEmpresas">
                                <thead>
                                    <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">NOMBRE</th>
                                    <th scope="col">ACCIONES</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    foreach($empresas as $empresa){ ?>                                        
                                        <tr id="<?= $empresa->idEmpresa; ?>">                                     
                                            <th scope="row" title="Ver empleados de <?= $empresa->nombreEmpresa; ?>" onclick="clickempresa(<?= $empresa->idEmpresa; ?>)"><?= $empresa->idEmpresa; ?></th>
                                            <td title="Ver empleados de <?= $empresa->nombreEmpresa; ?>" onclick="clickempresa(<?= $empresa->idEmpresa; ?>)"><?=$empresa->nombreEmpresa; ?></td>
                                        
                                        
                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalVerEmpresa" data-idempresa="<?= $empresa->idEmpresa; ?>" data-nombreempresa="<?= $empresa->nombreEmpresa; ?>" data-nit="<?= $empresa->nit; ?>" data-telefono="<?= $empresa->telefono; ?>"
                                            data-direccion="<?= $empresa->direccion; ?>" data-municipio="<?= $empresa->municipio; ?>" data-departamento="<?= $empresa->departamento; ?>"
                                            >Ver</button>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEditarEmpresa" data-idempresa="<?= $empresa->idEmpresa; ?>" data-nombreempresa="<?= $empresa->nombreEmpresa; ?>" data-nit="<?= $empresa->nit; ?>" data-telefono="<?= $empresa->telefono; ?>"
                                            data-direccion="<?= $empresa->direccion; ?>" data-municipio="<?= $empresa->municipio; ?>" data-departamento="<?= $empresa->departamento; ?>"
                                            >Editar</button>
                                            <button type="button" class="btn btn-danger" onclick="borrar(<?= $empresa->idEmpresa; ?>)">Eliminar</button>
                                        </td>                                                                           
                                        </tr>                                        
                              <?php }
                                ?>                                    
                                   
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">Empleados
                        <button type="button" class="btn btn-primary float-right"  data-toggle="modal" data-target="#modalNuevoEmpleado">+ Nuevo</button>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">NOMBRE</th>
                                    <th scope="col">ACCIONES</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    foreach($empleados as $empleado){ ?>                                        
                                        <tr id="e<?= $empleado->idEmpleado; ?>">
                                            <th scope="row"><?= $empleado->idEmpleado; ?></th>
                                            <td><?= $empleado->nombres." ".$empleado->apellidos;?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalVerEmpleado" data-id="<?= $empleado->idEmpleado; ?>" data-nombres="<?= $empleado->nombres; ?>" data-nit="<?= $empleado->nit; ?>" data-dui="<?= $empleado->dui; ?>"
                                                data-estado="<?= $empleado->estado; ?>" data-apellidos="<?= $empleado->apellidos; ?>" data-empresa="<?= $empleado->Empresas_idEmpresa; ?>"  data-rol="<?= $empleado->Roles_idRol; ?>"
                                                >Ver</button>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEditarEmpleado" data-id="<?= $empleado->idEmpleado; ?>" data-nombres="<?= $empleado->nombres; ?>" data-nit="<?= $empleado->nit; ?>" data-dui="<?= $empleado->dui; ?>"
                                                data-estado="<?= $empleado->estado; ?>" data-apellidos="<?= $empleado->apellidos; ?>" data-empresa="<?= $empleado->Empresas_idEmpresa; ?>"  data-rol="<?= $empleado->Roles_idRol; ?>"
                                                >Editar</button>
                                                <button type="button" class="btn btn-danger" onclick="borrarEmpleado(<?= $empleado->idEmpleado; ?>)">Eliminar</button>
                                            </td>                                    
                                        </tr>                                        
                              <?php }
                                ?>                                    
                                   
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">Roles
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalNuevoRol">+ Nuevo</button>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">NOMBRE ROL</th>
                                    <th scope="col">ACCIONES</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    foreach($roles as $rol){ ?>                                        
                                        <tr id="r<?= $rol->idRol; ?>">
                                            <th scope="row"><?= $rol->idRol; ?></th>
                                            <td><?= $rol->nombreRol;?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalVerRol" data-idrol="<?= $rol->idRol; ?>" data-nombrerol="<?= $rol->nombreRol; ?>" data-descripcionrol="<?= $rol->descripcionRol; ?>" data-permisos="<?= $rol->permisos; ?>"
                                                data-empresas_idempresa="<?= $rol->Empresas_idEmpresa; ?>" 
                                                >Ver</button>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEditarRol" data-idrol="<?= $rol->idRol; ?>" data-nombrerol="<?= $rol->nombreRol; ?>" data-descripcionrol="<?= $rol->descripcionRol; ?>" data-permisos="<?= $rol->permisos; ?>"
                                                data-empresas_idempresa="<?= $rol->Empresas_idEmpresa; ?>" 
                                                >Editar</button>
                                                <button type="button" class="btn btn-danger" onclick="borrarRol(<?= $rol->idRol; ?>)">Eliminar</button>
                                            </td>                                    
                                        </tr>                                        
                              <?php }
                                ?>                                    
                                   
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>
            
                
            </div>
        </div>
    </div>
