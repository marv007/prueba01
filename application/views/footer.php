        
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        <!--script src="<?//=base_url('assets/js/main.js');?>"></script-->
        
        <script type="text/javascript">
            function verEmpresa(idEmpresa) {
                
            }
            $('#modalVerEmpresa').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idEmpresa = button.data('idempresa') // Extract info from data-* attributes
                var nombreEmpresa = button.data('nombreempresa')
                var nit = button.data('nit')
                var telefono = button.data('telefono')
                var direccion = button.data('direccion')
                var municipio = button.data('municipio')
                var departamento = button.data('departamento')
                
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#id').val(idEmpresa)
                modal.find('#nombre').val(nombreEmpresa)
                modal.find('#nit').val(nit)
                modal.find('#telefono').val(telefono)
                modal.find('#direccion').val(direccion)
                modal.find('#municipio').val(municipio)
                modal.find('#departamento').val(departamento)

               // modal.find('.modal-body input').val(recipient)
            })

            //editarEmpresa
            $('#modalEditarEmpresa').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idEmpresa = button.data('idempresa') // Extract info from data-* attributes
                var nombreEmpresa = button.data('nombreempresa')
                var nit = button.data('nit')
                var telefono = button.data('telefono')
                var direccion = button.data('direccion')
                var municipio = button.data('municipio')
                var departamento = button.data('departamento')
                
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#id').val(idEmpresa)
                modal.find('#nombre').val(nombreEmpresa)
                modal.find('#nit').val(nit)
                modal.find('#telefono').val(telefono)
                modal.find('#direccion').val(direccion)
                modal.find('#municipio').val(municipio)
                modal.find('#departamento').val(departamento)

               // modal.find('.modal-body input').val(recipient)
            });

            //ver empleado
            $('#modalVerEmpleado').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id') // Extract info from data-* attributes
                var nombres = button.data('nombres')
                var nit = button.data('nit')
                var apellidos = button.data('apellidos')
                var dui = button.data('dui')
                var empresa = button.data('empresa')
                var estado = button.data('estado')
                var rol = button.data('rol')
                
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#id').val(id)
                modal.find('#nombres').val(nombres)
                modal.find('#nit').val(nit)
                modal.find('#dui').val(dui)
                modal.find('#empresa').val(empresa)
                modal.find('#apellidos').val(apellidos)
                modal.find('#estado').val(estado)
                modal.find('#rol').val(rol)

               // modal.find('.modal-body input').val(recipient)
            });


            //editar empleado
            $('#modalEditarEmpleado').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id') // Extract info from data-* attributes
                var nombres = button.data('nombres')
                var nit = button.data('nit')
                var apellidos = button.data('apellidos')
                var dui = button.data('dui')
                var empresa = button.data('empresa')
                var estado = button.data('estado')
                var rol = button.data('rol')
                
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#id').val(id)
                modal.find('#nombres').val(nombres)
                modal.find('#nit').val(nit)
                modal.find('#dui').val(dui)
                modal.find('#empresa').val(empresa)
                modal.find('#apellidos').val(apellidos)
                modal.find('#estado').val(estado)
                modal.find('#rol').val(rol)

               // modal.find('.modal-body input').val(recipient)
            });

             //ver rol
             $('#modalVerRol').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idRol = button.data('idrol') // Extract info from data-* attributes
                var nombreRol = button.data('nombrerol')
                var descripcionRol = button.data('descripcionrol')
                var permisos = button.data('permisos')
                var idEmpresa = button.data('empresas_idempresa')
                
                
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#idRol').val(idRol)
                modal.find('#nombreRol').val(nombreRol)
                modal.find('#descripcionRol').val(descripcionRol)
                modal.find('#permisos').val(permisos)
                modal.find('#Empresas_idEmpresa').val(idEmpresa)
                

               // modal.find('.modal-body input').val(recipient)
            });

            //editar rol
            $('#modalEditarRol').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idRol = button.data('idrol') // Extract info from data-* attributes
                var nombreRol = button.data('nombrerol')
                var descripcionRol = button.data('descripcionrol')
                var permisos = button.data('permisos')
                var idEmpresa = button.data('empresas_idempresa')
                
                
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('#idRol').val(idRol)
                modal.find('#nombreRol').val(nombreRol)
                modal.find('#descripcionRol').val(descripcionRol)
                modal.find('#permisos').val(permisos)
                modal.find('#Empresas_idEmpresa').val(idEmpresa)
                

               // modal.find('.modal-body input').val(recipient)
            });

            function clickempresa(idEmpresa){
                location.href ="http://localhost/prueba01/index.php/Dashboard/empresa/"+idEmpresa;
            }
            /*
            (function($){
                $("#frmNuevaEmpresa").submit(function(ev)){
                    ev.preventDefault();
                    $.ajax({
                        url: 'Dasboard/guardarEmpresa/',
                        type: 'POST',
                        data: $(this).serialize(),
                        
                    });
                }
            })(jQuery)*/
        </script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script>
            (function($){
                //Guardar nueva Empresa
                $("#frmNuevaEmpresa").submit(function(ev){
                    ev.preventDefault();
                    
                    $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/guardarEmpresa/',
                        type: 'POST',
                        data: $(this).serialize(),
                    });
                    alert("Se guardó con éxito");
                    location.href="http://localhost/prueba01/index.php/Dashboard";
                    
                    
                });
                //Editar Empresa
                $("#frmEditarEmpresa").submit(function(ev){
                    ev.preventDefault();
                    $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/editarEmpresa/',
                        type: 'POST',
                        data: $(this).serialize(),
                    });
                    alert("Se guardó con éxito");
                    location.href="http://localhost/prueba01/index.php/Dashboard";
                    
                });

                //Nuevo Empleado 
            $("#frmNuevoEmpleado").submit(function(ev){
                    ev.preventDefault();
                    $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/guardarEmpleado/',
                        type: 'POST',
                        data: $(this).serialize(),
                    });
                    alert("Se guardó con éxito");
                    location.href="http://localhost/prueba01/index.php/Dashboard";
                    
                }); 

                //editar Empleado 
            $("#frmEditarEmpleado").submit(function(ev){
                    ev.preventDefault();
                    $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/editarEmpleado/',
                        type: 'POST',
                        data: $(this).serialize(),
                    });
                    alert("Se guardó con éxito");
                    location.href="http://localhost/prueba01/index.php/Dashboard";
                    
                }); 

                //nuevo rol
            $("#frmNuevoRol").submit(function(ev){
                    ev.preventDefault();
                    $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/guardarRol/',
                        type: 'POST',
                        data: $(this).serialize(),
                    });
                    alert("Se guardó con éxito");
                    location.href="http://localhost/prueba01/index.php/Dashboard";
                    
                }); 

                //editar rol
            $("#frmEditarRol").submit(function(ev){
                    ev.preventDefault();
                    $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/editarRol/',
                        type: 'POST',
                        data: $(this).serialize(),
                    });
                    alert("Se guardó con éxito");
                    location.href="http://localhost/prueba01/index.php/Dashboard";
                    
                }); 

            })(jQuery) 

            function borrar($id){
                $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/eliminarEmpresa/'+$id,
                        type: 'GET',
                        data: $id,
                    });
                    $("#"+$id+"").remove();

            }

            function borrarEmpleado($id){
                $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/eliminarEmpleado/'+$id,
                        type: 'GET',
                        data: $id,
                    });
                    $("#e"+$id+"").remove();

            }

            function borrarRol($id){
                $.ajax({
                        url: 'http://localhost/prueba01/index.php/Dashboard/eliminarRol/'+$id,
                        type: 'GET',
                        data: $id,
                    });
                    $("#r"+$id+"").remove();

            }

            

            
            /*
            $(document).ready(function(){

                $("#frmNuevaEmpresa").submit(function(ev){
                
                id=$("#idEmpresa").val();
                nombre=$("#nombreEmpresa").val();

                if(id!="" && nombre!=""){

                    $.ajax({url:"<?php// echo base_url().'index.php/Dashboard/guardarEmpresa'; ?>",type:'POST',data:{idEmpresa:id,nombreEmpresa:nombre},success:function(result){
                    //$("#mens").html(result);

                    }});

                }else{

                    $("#mens").html("No deje campos vacíos");

                }

                });

                });
                */

        </script>




    </body>
</html>
