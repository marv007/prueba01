<table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">ID</th>
        <th scope="col">NOMBRE</th>
        <th scope="col">ACCIONES</th>                                    
        </tr>
    </thead>
    <tbody>
    <?php
        if(!$empleados){
            echo '<h3>Esta empresa no tiene empleados registrados</h3>';
        }else{
            foreach($empleados as $empleado){ ?>                                        
                <tr>
                    <th scope="row"><?= $empleado->idEmpleado; ?></th>
                    <td><?= $empleado->nombres." ".$empleado->apellidos;?></td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalVerEmpleado" data-id="<?= $empleado->idEmpleado; ?>" data-nombres="<?= $empleado->nombres; ?>" data-nit="<?= $empleado->nit; ?>" data-dui="<?= $empleado->dui; ?>"
                        data-estado="<?= $empleado->estado; ?>" data-apellidos="<?= $empleado->apellidos; ?>" data-empresa="<?= $empleado->Empresas_idEmpresa; ?>"  data-rol="<?= $empleado->Roles_idRol; ?>"
                        >Ver</button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEditarEmpleado" data-id="<?= $empleado->idEmpleado; ?>" data-nombres="<?= $empleado->nombres; ?>" data-nit="<?= $empleado->nit; ?>" data-dui="<?= $empleado->dui; ?>"
                        data-estado="<?= $empleado->estado; ?>" data-apellidos="<?= $empleado->apellidos; ?>" data-empresa="<?= $empleado->Empresas_idEmpresa; ?>"  data-rol="<?= $empleado->Roles_idRol; ?>"
                        >Editar</button>
                        <button type="button" class="btn btn-danger">Eliminar</button>
                    </td>                                    
                </tr>                                        
        <?php }
            
        } ?> 
                                       
        
    </tbody>
</table>