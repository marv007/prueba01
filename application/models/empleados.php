<?php 
class Empleados extends CI_Model {
    function _construct(){
        parent::__construct();
        $this->load->database();
    }

    public function obtenerTodos(){        
        $query = $this->db->get('empleados');

        if($query->num_rows() > 0){
            foreach ($query->result() as $fila) {
            $data[] = $fila;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function obtenerPorEmpresa($idEmpresa){
        $this->db->select('*');
        $this->db->from('empleados');
        $this->db->where('Empresas_idEmpresa', $idEmpresa);       
        $query = $this->db->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $fila) {
            $data[] = $fila;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function guardar(){
        
        $datos = array(
           'idEmpleado' => $this->input->post('idEmpleado'),
           'nombres' => $this->input->post('nombres'),
           'nit' => $this->input->post('nit'),
           'dui' => $this->input->post('dui'),
           'apellidos' => $this->input->post('apellidos'),
           'estado' => $this->input->post('estado'),
           'Empresas_idEmpresa' => $this->input->post('Empresas_idEmpresa'),
           'Roles_idRol' => $this->input->post('Roles_idRol')
           
        );
        
        $this->db->insert('empleados', $datos);
        
    }

    public function actualizar(){
        $datos = array(           
            'nombres' => $this->input->post('nombres'),
            'nit' => $this->input->post('nit'),
            'dui' => $this->input->post('dui'),
            'apellidos' => $this->input->post('apellidos'),
            'estado' => $this->input->post('estado'),
            'Empresas_idEmpresa' => $this->input->post('Empresas_idEmpresa'),
            'Roles_idRol' => $this->input->post('Roles_idRol')
        );
        $idem = $this->input->post('idEmpleado');          
        $this->db->where('idEmpleado', $idem);
        $this->db->update('empleados', $datos);
         
    }

    public function eliminar($id){
        $datos = array(  
            'idEmpleado' => $id
        );   
        $this->db->delete('empleados', $datos);
        
    
    }

}
?>