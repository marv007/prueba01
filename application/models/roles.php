<?php 
class Roles extends CI_Model {
    function _construct(){
        parent::__construct();
        $this->load->database();
    }

    public function obtenerTodos(){        
        $query = $this->db->get('roles');

        if($query->num_rows() > 0){
            foreach ($query->result() as $fila) {
            $data[] = $fila;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function guardar(){
        
        $datos = array(
           'idRol' => $this->input->post('idRol'),
           'nombreRol' => $this->input->post('nombreRol'),
           'descripcionRol' => $this->input->post('descripcionRol'),
           'permisos' => $this->input->post('permisos'),           
           'Empresas_idEmpresa' => $this->input->post('Empresas_idEmpresa'),
           
        );
        
        $this->db->insert('roles', $datos);
        
    }

    public function actualizar(){
        $datos = array(           
            'nombreRol' => $this->input->post('nombreRol'),
           'descripcionRol' => $this->input->post('descripcionRol'),
           'permisos' => $this->input->post('permisos'),           
           'Empresas_idEmpresa' => $this->input->post('Empresas_idEmpresa'),
        );
        $idRol = $this->input->post('idRol');          
        $this->db->where('idRol', $idRol);
        $this->db->update('roles', $datos);
         
    }

    public function eliminar($id){
        $datos = array(  
            'idRol' => $id
        );   
        $this->db->delete('roles', $datos);
        
    
    }

}
?>