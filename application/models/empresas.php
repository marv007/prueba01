<?php 
class Empresas extends CI_Model {
    function _construct(){
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function obtenerTodos(){        
        $query = $this->db->get('empresas');

        if($query->num_rows() > 0){
            foreach ($query->result() as $fila) {
            $data[] = $fila;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function guardar(){

        
        $datos = array(
           'idEmpresa' => $this->input->post('idEmpresa'),
           'nombreEmpresa' => $this->input->post('nombreEmpresa'),
           'nit' => $this->input->post('nit'),
           'telefono' => $this->input->post('telefono'),
           'direccion' => $this->input->post('direccion'),
           'municipio' => $this->input->post('municipio'),
           'departamento' => $this->input->post('departamento'),
           
        );
        
        $this->db->insert('empresas', $datos);
        
    }

    public function actualizar(){
        $datos = array(           
           'nombreEmpresa' => $this->input->post('nombreEmpresa'),
           'nit' => $this->input->post('nit'),
           'telefono' => $this->input->post('telefono'),
           'direccion' => $this->input->post('direccion'),
           'municipio' => $this->input->post('municipio'),
           'departamento' => $this->input->post('departamento'),
           
        );
        $idem = $this->input->post('idEmpresa');          
        $this->db->where('idEmpresa', $idem);
        $this->db->update('empresas', $datos);
         
    }

    public function eliminar($id){
        $datos = array(  
            'idEmpresa' => $id
        );   
        $this->db->delete('empresas', $datos);
        
    
}

    
}
?>