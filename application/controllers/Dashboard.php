<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->model('empresas');
		$this->load->model('empleados');
		$this->load->model('roles');

	}
	public function index(){
		
		$data['empresas'] = $this->empresas->obtenerTodos();
		$data['empleados'] = $this->empleados->obtenerTodos();
		$data['roles'] = $this->roles->obtenerTodos();
		
		$this->load->view('header');
		$this->load->view('vista', $data);
		$this->load->view('modales', $data);		
        $this->load->view('footer');
	}

	public function empresa($idEmpresa){
		$data['empleados'] = $this->empleados->obtenerPorEmpresa($idEmpresa);
		$data['empresas'] = $this->empresas->obtenerTodos();		
		$data['roles'] = $this->roles->obtenerTodos();

		$this->load->view('header');
		$this->load->view('empleadosPorEmpresa', $data);
		$this->load->view('modales', $data);
		$this->load->view('footer');

		
	}

	public function guardarEmpresa(){		
		$this->empresas->guardar();
				
	}

	public function editarEmpresa(){		
		$this->empresas->actualizar();
				
	}

	public function eliminarEmpresa($id){		
		$this->empresas->eliminar($id);		
	}

	public function guardarEmpleado(){		
		$this->empleados->guardar();
				
	}

	public function editarEmpleado(){		
		$this->empleados->actualizar();		
	}

	public function eliminarEmpleado($id){		
		$this->empleados->eliminar($id);		
	}
	public function guardarRol(){		
		$this->roles->guardar();		
	}

	public function editarRol(){		
		$this->roles->actualizar();		
	}
	public function eliminarRol($id){		
		$this->roles->eliminar($id);		
	}
}
